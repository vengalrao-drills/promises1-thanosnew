const fs = require("fs").promises;
const path = require("path");

const allCards = (listID) => {
  return new Promise((resolve, reject) => {
    const filePath = path.join(__dirname, "data", "cards_1.json");
    fs.readFile(filePath, "utf-8")
      .then((data) => {
        let cardsData = JSON.parse(data);
        // console.log(typeof cardsData)

        cardsData = Object.entries(cardsData); // cards data is in object . so converting to array.
        // console.log(cardsData)

        // Finding the cards associated with the given list ID in the array
        let finalAns = cardsData.reduce((accumulator, current) => {
          if (current[0] == listID) {
            // ,matching the ids and assigning it to the accumulator
            accumulator = current[1]; // assigning the cards data to the accumulator
          }
          return accumulator;
        }, []);
        resolve(finalAns); // Resolving the promise
      })
      .catch((err) => {
        reject(err); // Rejecting the promise if an error occurs
      });
  });
};
module.exports = allCards;
// allCards("qwsa221")
//   .then((data) => {
//     console.log(data);
//   })
//   .catch((error) => {
//     console.log(error);
//   });

// const allCards = require("../callback3.js");
// allCards("qwsa221", (err, data) => {
//   if (err) {
//     console.log(err);
//   } else {
//     console.log("testId :- qwsa221\n cards", data);
//   }
// });
