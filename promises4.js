// Problem 4: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

//     Get information from the Thanos boards
//     Get all the lists for the Thanos board
//     Get all cards for the Mind list simultaneously

const fs = require("fs").promises;
const path = require("path");

const previousFunction = (word, cardWordz) => {
  return new Promise((resolve, reject) => {
    let boardsPath = path.join(__dirname, "data", "boards_1.json");
    let finalAns = []; // this is the final answer- where I am storing answers.

    //     Get information from the Thanos boards
    fs.readFile(boardsPath, "utf-8")
      .then((data) => {
        let boardsData = JSON.parse(data); //Parse the JSON data from the file
        let infoBoard = boardsData.reduce((accumulator, current) => {
          // console.log(current.name)
          if (current.name == word) {
            // comparing the word to the current name-> data from boards
            accumulator = current;
          }
          return accumulator;
        }, {});
        let value = { ...infoBoard }; // here destructuring. if i can't directly asssign- i can stringify the value - so destructing. and assigning
        // finalAns.push(infoBoard)
        finalAns.push(JSON.stringify(value));

        // Get all the lists for the Thanos board
        let heroId = infoBoard.id; // getting the id from the boards_1.json.

        let listsPath = path.join(__dirname, "data", "lists_1.json");
        fs.readFile(listsPath, "utf-8")
          .then((data) => {
            let listsData = JSON.parse(data); //Parse the JSON data from the file
            listsData = Object.entries(listsData);
            // console.log(listsData)
            // extracting cards information based on the cardId
            let infoLists = listsData.reduce((accumulator, current) => {
              if (current[0] == heroId) {
                // comparing hero id
                accumulator = current[1];
              }
              return accumulator;
            }, {});

            let value1 = [...infoLists];

            finalAns.push(JSON.stringify(value1));
            // console.log(infoLists)

            let cardsPath = path.join(__dirname, "data", "cards_1.json");
            let herosArray = infoLists;

            // Reading cards data and finding the cardId based on the cardWordz
            fs.readFile(cardsPath, "utf-8")
              .then((data) => {
                let cardId = herosArray.reduce((accumulator, current) => {
                  if (current.name.toLowerCase() == cardWordz.toLowerCase()) {
                    accumulator = current.id;
                  }
                  return accumulator;
                }, 0);
                let cardsData = JSON.parse(data); //Parse the JSON data from the file
                cardsData = Object.entries(cardsData);
                // console.log(cardsData)

                let cardsAns = cardsData.reduce((accumulator, current) => {
                  if (current[0] == cardId) {
                    accumulator = current[1];
                  }
                  return accumulator;
                }, []);
                let value = cardsAns;
                finalAns.push(JSON.stringify(value));
                // console.log(cardsAns)
                resolve(finalAns);
              })
              .catch((err) => {
                reject(err);
              });
          })
          .catch((err) => {
            reject(err);
          });
      })
      .catch((err) => {
        reject(err);
      });
  });
};

module.exports = previousFunction;
// previousFunction("Thanos", "Mind")
//   .then((data) => {
//     console.log(data);
//   })
//   .catch((err) => {
//     console.log(err);
//   });
