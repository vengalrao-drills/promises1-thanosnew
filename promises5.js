/* 
    Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/

const fs = require("fs").promises;
const path = require("path");

const previousFunction = (word, cardWordz) => {
    return new Promise((resolve, reject) => {
        let boardsPath = path.join(__dirname, "data", "boards_1.json");
        let finalAns = [];
        // Reading boards data
        fs.readFile(boardsPath, "utf-8").then((data) => {
            let boardsData = JSON.parse(data);
            // Extracting board information based on the provided word
            let infoBoard = boardsData.reduce((accumulator, current) => {
                // console.log(current.name)
                if (current.name == word) {
                    accumulator = current;
                }
                return accumulator;
            }, {});
            let value = { ...infoBoard };
            // finalAns.push(infoBoard)
            finalAns.push(JSON.stringify(value));

            // Get all the lists for the Thanos board
            let heroId = infoBoard.id; // getting the id from the boards_1.json.

            let listsPath = path.join(__dirname, "data", "lists_1.json");
            // Reading lists data
            fs.readFile(listsPath, "utf-8").then((data) => {
                let listsData = JSON.parse(data);
                listsData = Object.entries(listsData);
                // console.log(listsData)
                let infoLists = listsData.reduce((accumulator, current) => {
                    if (current[0] == heroId) {
                        accumulator = current[1];
                    }
                    return accumulator;
                }, {});

                let value1 = [...infoLists];

                finalAns.push(JSON.stringify(value1));
                // console.log(infoLists)

                let cardsPath = path.join(__dirname, "data", "cards_1.json");
                let herosArray = infoLists;
                // console.log(herosArray)
                // console.log( herosArray)

                // Reading cards data
                fs.readFile(cardsPath, "utf-8").then((data) => {
                    cardWordz = cardWordz.split("and");
                    // console.log(cardWordz)
                    // Finding cardIds based on cardWordz
                    let cardIds = cardWordz.reduce((accumulator, element) => {
                        let cardId = herosArray.reduce((accumulator, current) => {
                            let word1 = current.name.trim().toLowerCase();
                            let word2 = element.trim().toLowerCase();
                            if (word1 == word2) {
                                accumulator = current.id;
                            }
                            return accumulator;
                        }, []);
                        accumulator.push(cardId);
                        return accumulator;
                    }, []);
                    // console.log(cardIds)

                    let cardsData = JSON.parse(data);
                    cardsData = Object.entries(cardsData);
                    // console.log(cardsData)
                    // Extracting cards information based on cardIds
                    let lastData = cardIds.reduce(
                        (Cardsaccumulator, CardsCurrent) => {
                            let cardsAns = cardsData.reduce(
                                (accumulator, current) => {
                                    let cardId = CardsCurrent;
                                    if (current[0] == cardId) {
                                        accumulator = current[1];
                                    }
                                    return accumulator;
                                },
                                []
                            );
                            Cardsaccumulator[CardsCurrent] = cardsAns;
                            return Cardsaccumulator;
                        },
                        {}
                    );
                    // console.log(cardsAns)
                    //
                    let value = lastData;
                    finalAns.push(JSON.stringify(value));
                    // console.log(cardsAns)
                    resolve(finalAns)
                }).catch((err) => {
                    console.log(err)
                })
            }).catch((err) => {
                console.log(err)
            })
        }).catch((err) => {
            reject(err)
        })

    })

}
// previousFunction("Thanos",'Mind and Space' ,(err, data) => {
//   if (err) {
//     console.log(err);
//   } else {
//     console.log(data);
//   }
// });
module.exports =previousFunction ;
// previousFunction("Thanos", 'Mind and Space').then((data) => {
//     console.log(data)
// }).catch((err) => {
//     console.log(err)
// })