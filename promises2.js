// Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.

const fs = require("fs").promises;
const path = require("path");

const allListsOfId = (boardId) => {

    return new Promise((resolve, reject) => {
        const filePath = path.join(__dirname, "data", "lists_1.json");
        fs.readFile(filePath, "utf-8").then((data) => {
            let listData = JSON.parse(data);   //converting a JSON-formatted string into a JavaScript object.
            listData = Object.entries(listData);
            // console.log(listData)
            let findAns = listData.reduce((accumulator, current) => {
                if (current[0] == boardId) { //matching the id's
                    accumulator = JSON.stringify(current[1]);
                }
                return accumulator;
            }, {});

            resolve(findAns)  // Resolving the promise
        }).catch((err) => {
            reject(err)  // Rejecting the promise if an error occurs
        })
    })

};
module.exports = allListsOfId;
// allListsOfId("mcu453ed").then((data) => {
//     console.log(data)
// }).catch((error) => {
//     console.log(error)
// })

// allListsOfId("mcu453ed", (err, data) => {
//   if (err) {
//     console.log(err);
//   } else {
//     console.log(data);
//   }
// });
