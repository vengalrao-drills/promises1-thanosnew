const previousFunction = require("../promises5")

previousFunction("Thanos", 'Mind and Space').then((data) => {
    console.log(data)
}).catch((err) => {
    console.log(err)
})