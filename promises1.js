// Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.

const fs = require("fs").promises;
const path = require("path");


const getBoardsInformation = (boardId) => {
    return new Promise((resolve, reject) => {
        const filepath = path.join(__dirname, "data", "boards_1.json");
        fs.readFile(filepath, "utf-8").then((data) => {
            const boardsData = JSON.parse(data);
            // finding the board with the given ID in the array
            let id = boardsData.reduce((accumulator, current) => {
                if (current.id == boardId) { // if borad id == current id
                    accumulator = current; // then assign it to the accumulator
                }
                return accumulator;
            }, {});
            // converting the result to a JSON string
            id = JSON.stringify(id);
            resolve(id) // sending the id . Now Promise is sended. true value

        }).catch((err) => {
            reject(err)  // Rejecting the promise if an error occurs
        })
    })

    // console.log(filepath)


};

module.exports = getBoardsInformation;
// getBoardsInformation('mcu453ed').then((data) => {
//     console.log(data)
// }).catch((err) => {
//     console.log(err)
// })