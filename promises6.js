/* 
    Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/

const fs = require("fs").promises;
const path = require("path");


const previousFunction = (word, callback) => {
    return new Promise((resolve, reject) => {
        let boardsPath = path.join(__dirname, "data", "boards_1.json");
        let finalAns = [];

        //     Get information from the Thanos boards
        fs.readFile(boardsPath, "utf-8").then((data) => {
            let boardsData = JSON.parse(data);
            // Extracting board information based on the provided word
            let infoBoard = boardsData.reduce((accumulator, current) => {
                // console.log(current.name)
                if (current.name == word) {
                    accumulator = current;
                }
                return accumulator;
            }, {});
            let value = { ...infoBoard };
            // finalAns.push(infoBoard)
            finalAns.push(JSON.stringify(value));

            // Get all the lists for the Thanos board
            let heroId = infoBoard.id; // getting the id from the boards_1.json.

            let listsPath = path.join(__dirname, "data", "lists_1.json");
            fs.readFile(listsPath, "utf-8").then((data) => {
                // console.log(data)
                let listsData = JSON.parse(data);
                listsData = Object.entries(listsData);
                // console.log(listsData)
                let infoLists = listsData.reduce((accumulator, current) => {
                    if (current[0] == heroId) {
                        accumulator = current[1];
                    }
                    return accumulator;
                }, {});

                let value1 = [...infoLists];

                finalAns.push(JSON.stringify(value1));
                // console.log(infoLists)

                let cardsPath = path.join(__dirname, "data", "cards_1.json");
                let herosArray = infoLists;
                // Reading cards data
                fs.readFile(cardsPath, "utf-8").then((data) => {
                    // console.log(data)
                    let cardsData = JSON.parse(data);
                    cardsData = Object.entries(cardsData);
                    //console.log(cardsData)
                    // console.log(cardsData)
                    // Extracting cards information based on herosArray (lists)
                    let lastData = herosArray.reduce(
                        (heroaccumulator, herocurrent) => {
                            let dataOfCard = cardsData.reduce(
                                (accumulator, current) => {
                                    if (current[0] == herocurrent.id) {
                                        //console.log(current[0], herocurrent.id)
                                        accumulator = current[1];
                                    }
                                    return accumulator;
                                },
                                []
                            );
                            // console.log(dataOfCard)
                            if (dataOfCard.length > 0) {
                                heroaccumulator[herocurrent.id] = dataOfCard;
                            }

                            return heroaccumulator;
                        },
                        {}
                    );
                    let value = { ...lastData };
                    // finalAns.push(lastData)
                    finalAns.push(JSON.stringify(value));
                    // console.log(lastData);

                    resolve(finalAns)
                }).catch((err) => {
                    console.log(err)
                })


            }).catch((err) => {
                console.log(err)
            })



        }).catch((err) => {
            console.log(err)
        })


    })
}

// previousFunction("Thanos").then((data) => {
//     console.log(data)
// }).catch((err) => {
//     console.log(err)
// })

module.exports = previousFunction;

// previousFunction("Thanos", (err, data) => {
//   if (err) {
//     console.log(err);
//   } else {
//     console.log(data);
//   }
// });
